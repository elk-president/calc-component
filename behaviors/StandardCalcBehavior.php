<?php
namespace elkpresident\calccomponent\behaviors;

use yii\base\Behavior;
use \NXP\MathExecutor;

class StandardCalcBehavior extends Behavior
{
    public function calc($argument)
    {
      $executor = new MathExecutor();

      return $executor->execute($argument);
    }
}
