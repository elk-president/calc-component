DIRECTORY STRUCTURE
-------------------

      behaviors/          contains component behavior classes
      config/             contains application configurations
      models/             contains model classes
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages

WORK WORK
------------
### Installation
~~~
composer require elk-president/calc-component:"dev-master"
~~~

### Testing
~~~
vendor/bin/codecept run
~~~

### Basic usage
~~~
use elkpresident\calccomponent\CalcComponent;

/** $input is your string to be parsed */
$calcComponent = new CalcComponent();
$calcComponent->processMathString($input)

/** In case you need custom behavior */

class YourCalcBehavior extends yii\base\Behavior
{
    public function calc($argument)
    {
      $executor = new YourCalculationClass();

      return $executor->yourCalculationClassMethod($argument);
    }
}

$calcComponent = new CalcComponent([
      'calcBehavior'  => YourCalcBehavior
  ]);
$calcComponent->processMathString($input)

/** Or in Yii app config */

'components' => [
  // ...
  'calc' => [
    'class' => 'elkpresident\calccomponent\CalcComponent',
    'calcBehavior' => YourCalcBehavior,
  ]
  // ...
]

~~~
