<?php
namespace elkpresident\calccomponent\tests\unit\models;

use elkpresident\calccomponent\CalcComponent;
use NXP\Exception\UnknownFunctionException;
use NXP\Exception\IncorrectBracketsException;
use NXP\Exception\IncorrectExpressionException;

class CalcComponentTest extends \Codeception\Test\Unit
{
        protected $tester;

    public $inputExceptionsMap = [
        '1 + 2 * (2 - (4+1'     => IncorrectBracketsException::class,
        '1 + 2 ** (2 - (4+1))'  => IncorrectExpressionException::class,
        'some text'             => UnknownFunctionException::class,
        ''                      => IncorrectExpressionException::class,
    ];

    public $inputExpectedOutputsMap = [
        '1 + 2 * (2 - (4+10))'  => -23,
        '4 * 78 - 1'            => 311,
        '3^7'                   => 2187,
    ];

    public function testCalcWrongInputs ()
    {
      foreach ($this->inputExceptionsMap as $input => $exceptionClass) {
          $this->tester->expectException($exceptionClass, function () use ($input) {
            $result = (new CalcComponent())->processMathString($input);
          });
      }
    }

    public function testCalcCorrectInputs ()
    {
        foreach ($this->inputExpectedOutputsMap as $input => $expectedOutput) {
            $this->tester->assertEquals((new CalcComponent())->processMathString($input), $expectedOutput);
        }
    }
}
