<?php
namespace elkpresident\calccomponent\behaviors;

use yii\base\Behavior;

class WolframCalcBehavior extends Behavior
{
    public function calc($argument)
    {
        return 42;
    }
}
