<?php
namespace elkpresident\calccomponent;

use yii\base\Component;
use yii\base\Behavior;
use elkpresident\calccomponent\behaviors\StandardCalcBehavior;

class CalcComponent extends Component
{
  /** May use custom behavior */
  public function __construct($config = [])
  {
      $behavior = isset($config['calcBehavior']) ? $config['calcBehavior']::className() : StandardCalcBehavior::className();

      $this->attachBehavior('calc', [
          'class' => $behavior,
      ]);
  }

  public function processMathString ($string)
  {
    return $this->calc($string);
  }
}
